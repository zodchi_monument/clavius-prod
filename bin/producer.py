# Third party import
from yaml import safe_load, YAMLError

# Local application imports
from broadcasts import PredictiveBroadcast, ControlBroadcast

# Открываем yaml файл с конфигурацией продьюсера
with open('config.yml', 'r') as yf:
    try:
        config = safe_load(yf)
    except YAMLError as exc:
        print(exc)

broadcast = config['type']
types = {'Predictive': PredictiveBroadcast, 'MPC': ControlBroadcast}
broadcast = types[broadcast]

for name in config['models']:
    m_conf = config['models'][name]
    id = m_conf['id']
    for th in m_conf['threads']:
        thread = broadcast(id=id, mode=th, **m_conf['threads'][th])
        thread.start()











