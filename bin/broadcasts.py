# Standard library imports
from abc import abstractmethod
from threading import Thread
from os import environ
import time
import json

# Third party import
from kafka import KafkaProducer
from mysql.connector.errorcode import ER_ACCESS_DENIED_ERROR, ER_BAD_DB_ERROR
from mysql import connector


class Broadcast(Thread):
    """Метакласс, описывающий общее поведение и структуру всех Broadcast методов"""
    def __init__(self, id, mode, sql):
        Thread.__init__(self)
        self.json = {'id': id, 'mode': mode, 'data': {}}
        self.sql = sql
        self._connect()
        self.kafka_producer = KafkaProducer(bootstrap_servers=[environ["KAFKA_HOST"]])
        self.kafka_topic = environ["KAFKA_TOPIC"]

    def _connect(self):
        conn_params = dict(host=environ['DB_HOST'], user=environ['DB_USER'], password=environ['DB_PASS'])
        try:
            self.conn = connector.connect(**conn_params)
        except connector.Error as err:
            if err.errno == ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)
        self.cursor = self.conn.cursor()
        self.db_name = environ['DB_NAME']
        try:
            self.cursor.execute(f"USE {self.db_name}")
        except connector.Error:
            print(f"Database '{self.db_name}' does not exist!")

    @abstractmethod
    def _preprocess_data(self):
        pass

    @abstractmethod
    def run(self):
        pass


class PredictiveBroadcast(Broadcast):

    def __init__(self, id, mode, delay, sql):
        Broadcast.__init__(self, id, mode, sql)
        self.delay = delay * 60

    def _preprocess_data(self):
        self.cursor.execute(self.sql)
        data = [i for i in self.cursor]

        start = data[0][0]
        self.json['data'] = {}
        self.json['data']['start'] = str(start)
        self.json['data']['tags'] = str([i[0] for i in self.cursor.description][1:])
        for i in range(len(data)):
            t = [(start - data[i][0]).seconds/60]
            for e in data[i][1:]:
                ads = float(e) if e is not None else None
                t.append(ads)
            data[i] = tuple(t)
        self.json['data']['values'] = data
        return bytes(json.dumps(self.json).replace(' ', ''), encoding='utf-8')

    def run(self):
        while True:
            start = time.time()
            json_to_send = self._preprocess_data()
            self.kafka_producer.send(self.kafka_topic, key=b"message", value=json_to_send)
            time.sleep(self.delay - ((time.time() - start) % self.delay))


class ControlBroadcast(Broadcast):

    def _preprocess_data(self):
        self.json['data']['values'] = 'test'
        return bytes(json.dumps(self.json), encoding='utf-8')

    def run(self):
        json_to_send = self._preprocess_data()
        self.kafka_producer.send(self.kafka_topic, key=b"message", value=json_to_send)

